package br.com.restassuredapitesting.tests.booking.tests;

import br.com.restassuredapitesting.suites.Acceptance;
import br.com.restassuredapitesting.suites.E2e;
import br.com.restassuredapitesting.tests.base.tests.BaseTest;
import br.com.restassuredapitesting.tests.booking.requests.DeleteBookingRequest;
import br.com.restassuredapitesting.tests.booking.requests.GetBookingRequest;
import br.com.restassuredapitesting.tests.booking.requests.PutBookingRequest;
import br.com.restassuredapitesting.utils.Utils;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

public class DeleteBookingTest extends BaseTest {

    DeleteBookingRequest deleteBookingRequest = new DeleteBookingRequest();
    GetBookingRequest getBookingRequest = new GetBookingRequest();

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(Acceptance.class)
    @DisplayName("Deletar uma reserva com token")
    public void deletarReservaComToken() throws Exception{

        int primeiroId = getBookingRequest.allBookings().then().statusCode(200).extract().path("[0].bookingid");
        deleteBookingRequest.deletarUmaReservaComToken(primeiroId, Utils.validPayloadBooking()).then().statusCode(201)
                .time(lessThan(10L), TimeUnit.SECONDS);
    }
    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar Delete sem autorizacao")

    public void validarDeleteSemAutorizacao() throws Exception{
        deleteBookingRequest.deletarReservaSemAutorizacao().then()
                .statusCode(404)
                .time(lessThan(10L), TimeUnit.SECONDS);
    }
    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar deletar uma reserva que nao existe")
    public void deletarReservaQueNãoExiste() throws Exception{
        int Id = 80;
        deleteBookingRequest.deletarUmaReservaComToken(Id, Utils.validPayloadBooking()).then().statusCode(405)
                .time(lessThan(10L), TimeUnit.SECONDS);

    }

}
