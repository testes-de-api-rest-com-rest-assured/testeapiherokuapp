package br.com.restassuredapitesting.tests.booking.tests;

import br.com.restassuredapitesting.suites.Contract;
import br.com.restassuredapitesting.tests.base.tests.BaseTest;
import br.com.restassuredapitesting.tests.booking.requests.GetBookingRequest;
import br.com.restassuredapitesting.tests.booking.requests.GetBookingRequestReservaEspecifica;
import br.com.restassuredapitesting.utils.Utils;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.lessThan;

public class GetBookingReservaEspecificaTest extends BaseTest {

    GetBookingRequestReservaEspecifica getBookingRequestReservaEspecifica = new GetBookingRequestReservaEspecifica();

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Garantir retorno de reserva especifica")
    public void garantirRetornoReservaEspecifica() throws Exception {
        getBookingRequestReservaEspecifica.reservaEspecifica().then()
                .statusCode(200)
                .assertThat()
                .body(matchesJsonSchema(new File(Utils.getContractsBasePath("booking", "bookingIdEspecifico"))));
    }


}
