package br.com.restassuredapitesting.tests.booking.tests;

import br.com.restassuredapitesting.suites.Acceptance;
import br.com.restassuredapitesting.suites.E2e;
import br.com.restassuredapitesting.tests.base.tests.BaseTest;
import br.com.restassuredapitesting.tests.booking.requests.GetBookingFiltrosRequest;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import junit.framework.Assert;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

public class GetBookingFiltrosTest extends BaseTest {

    GetBookingFiltrosRequest getBookingFiltrosRequest = new GetBookingFiltrosRequest();

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(Acceptance.class)
    @DisplayName("Listar Reserva pelo firstname")

    public void validarReservaFiltroFirstname() throws Exception{
        getBookingFiltrosRequest.reservaComFiltroFirstname().then()
                .statusCode(200)
                .time(lessThan(20L), TimeUnit.SECONDS)
                .body("size()", Matchers.greaterThan(0))
                ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(Acceptance.class)
    @DisplayName("Listar Reserva pelo lastname")

    public void validarReservaFiltroLastname() throws Exception{
        getBookingFiltrosRequest.reservaComFiltroLastname().then()
                .statusCode(200)
                .time(lessThan(20L), TimeUnit.SECONDS)
                .body("size()", Matchers.greaterThan(0))

        ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(Acceptance.class)
    @DisplayName("Listar Reserva pelo check in")

    public void validarReservaFiltroCheckIn() throws Exception{
        getBookingFiltrosRequest.reservaComFiltroCheckin().then()
                .statusCode(200)
                .time(lessThan(20L), TimeUnit.SECONDS)
                .body("size()", Matchers.greaterThan(0))
        ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(Acceptance.class)
    @DisplayName("Listar Reserva pelo check out")

    public void validarReservaFiltroCheckOut() throws Exception{
        getBookingFiltrosRequest.reservaComFiltroCheckout().then()
                .statusCode(200)
                .time(lessThan(20L), TimeUnit.SECONDS)
                .body("size()", Matchers.greaterThan(0))
        ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(Acceptance.class)
    @DisplayName("Listar Reserva pelo check in e check out")

    public void validarReservaFiltroCheckinCheckOut() throws Exception{
        getBookingFiltrosRequest.reservaComFiltroCheckinCheckout().then()
                .statusCode(200)
                .time(lessThan(20L), TimeUnit.SECONDS)
                .body("size()", Matchers.greaterThan(0))

        ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(Acceptance.class)
    @DisplayName("Listar Reserva pelos filtros firstname, check in e check out")

    public void validarReservaFiltroFirstnameCheckinCheckOut() throws Exception{
        getBookingFiltrosRequest.reservaComFiltroFirstnameCheckinCheckout().then()
                .statusCode(200)
                .time(lessThan(20L), TimeUnit.SECONDS)
                .body("size()", Matchers.greaterThan(0))
                ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar erro 500 por filtro mal formatado")

    public void validarErro500FiltroMalFormatado() throws Exception{
        getBookingFiltrosRequest.Erro500FiltroMalFormatado().then()
                .statusCode(500)
                .time(lessThan(20L), TimeUnit.SECONDS);
    }


}
