package br.com.restassuredapitesting.tests.booking.tests;

import br.com.restassuredapitesting.suites.Acceptance;
import br.com.restassuredapitesting.suites.E2e;
import br.com.restassuredapitesting.tests.base.tests.BaseTest;
import br.com.restassuredapitesting.tests.booking.requests.GetBookingRequest;
import br.com.restassuredapitesting.tests.booking.requests.PutBookingRequest;
import br.com.restassuredapitesting.utils.Utils;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

@Feature("Reservas")
public class PutBookingTest extends BaseTest {
    PutBookingRequest putBookingRequest = new PutBookingRequest();
    GetBookingRequest getBookingRequest = new GetBookingRequest();

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(Acceptance.class)
    @DisplayName("Alterar uma reserva com token")
    public void validarAlterarUmaReservaComToken() throws Exception{

        int primeiroId = getBookingRequest.allBookings().then().statusCode(200).extract().path("[0].bookingid");
        putBookingRequest.alterarUmaReservaComToken(primeiroId, Utils.validPayloadBooking()).then().statusCode(200)
                .time(lessThan(10L), TimeUnit.SECONDS)
                .body("size()", greaterThan(0));

    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar alterar uma reserva sem token")

    public void validaralterarUmaReservaSemToken() throws Exception{
        putBookingRequest.alterarUmaReservaSemToken().then()
                .statusCode(403)
                .time(lessThan(10L), TimeUnit.SECONDS);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar alterar uma reserva com token no formato invalido")
    public void validarAlterarUmaReservaComTokenInvalido() throws Exception{

        int primeiroId = getBookingRequest.allBookings().then().statusCode(200).extract().path("[0].bookingid");
        putBookingRequest.alterarUmaReservaComTokenInvalido(primeiroId, Utils.validPayloadBooking()).then().statusCode(403)
                .time(lessThan(10L), TimeUnit.SECONDS);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar alterar uma reserva com Basic")

    public void validaralterarUmaReservaComBasic() throws Exception{
        int primeiroId = getBookingRequest.allBookings().then().statusCode(200).extract().path("[0].bookingid");
        putBookingRequest.alterarUmaReservaComToken(primeiroId, Utils.validPayloadBooking()).then().statusCode(200)
                .time(lessThan(10L), TimeUnit.SECONDS)
                .body("size()", greaterThan(0));
    }
}
