package br.com.restassuredapitesting.tests.booking.requests;

import io.qameta.allure.Step;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class HealthcheckRequest {

    @Step("Buscar API")
    public Response buscarAPI(){
        return given()
                .when()
                .get("ping")
                .prettyPeek();
    }
}
