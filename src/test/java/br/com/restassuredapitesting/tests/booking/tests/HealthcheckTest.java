package br.com.restassuredapitesting.tests.booking.tests;

import br.com.restassuredapitesting.suites.Healthcheck;
import br.com.restassuredapitesting.tests.base.tests.BaseTest;
import br.com.restassuredapitesting.tests.booking.requests.HealthcheckRequest;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.lessThan;

public class HealthcheckTest extends BaseTest {

    HealthcheckRequest healthcheckRequest = new HealthcheckRequest();

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Healthcheck.class)
    @DisplayName("Validar que API esta online")

    public void validarAPIOnline() throws Exception{
        healthcheckRequest.buscarAPI().then()
                .statusCode(201)
                .assertThat()
                .time(lessThan(10L), TimeUnit.SECONDS);
    }

}
