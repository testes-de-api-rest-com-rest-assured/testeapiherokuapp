package br.com.restassuredapitesting.tests.booking.requests;

import br.com.restassuredapitesting.tests.auth.requests.PostAuthInvalidoRequest;
import br.com.restassuredapitesting.tests.auth.requests.PostAuthRequest;
import br.com.restassuredapitesting.tests.auth.tests.PostAuthTest;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class PutBookingRequest {

    PostAuthRequest postAuthRequest = new PostAuthRequest();
    PostAuthInvalidoRequest postAuthInvalidoRequest = new PostAuthInvalidoRequest();

    @Step("Alterar uma reserva com token")
    public Response alterarUmaReservaComToken(int id, JSONObject payload){
        return given()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("Cookie", postAuthRequest.getToken())
                .when()
                .body(payload.toString())
                .put("booking/" + id)
                ;
    }

    @Step("Alterar uma reserva sem token")
    public Response alterarUmaReservaSemToken(){
        return given()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .when()
                .put("booking/10")
                ;
    }

    @Step("Alterar uma reserva com token no formato invalido")
    public Response alterarUmaReservaComTokenInvalido(int id, JSONObject payload){
        return given()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("Cookie", postAuthInvalidoRequest.getToken())
                .when()
                .body(payload.toString())
                .put("booking/" + id)
                ;
    }

    @Step("Alterar uma reserva com Basic")
    public Response alterarUmaReservaComBasic(int id, JSONObject payload){
        return given()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("Basic", "YWRtaW46cGFzc3dvcmQxMjM=]")
                .when()
                .body(payload.toString())
                .put("booking/" + id)
                ;
    }

}
