package br.com.restassuredapitesting.tests.booking.requests;

import io.qameta.allure.Step;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class GetBookingRequestReservaEspecifica {

    @Step("Buscar reserva especifica")
    public Response reservaEspecifica(){
        return given()
                .when()
                .get("booking/1")
                .prettyPeek();
    }
}
