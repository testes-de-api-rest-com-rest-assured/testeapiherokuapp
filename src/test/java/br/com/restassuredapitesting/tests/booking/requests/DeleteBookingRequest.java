package br.com.restassuredapitesting.tests.booking.requests;

import br.com.restassuredapitesting.tests.auth.requests.PostAuthRequest;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class DeleteBookingRequest {

    PostAuthRequest postAuthRequest = new PostAuthRequest();

    @Step("Deletar uma reserva com Token")
    public Response deletarUmaReservaComToken(int id, JSONObject payload){
        return given()
                .header("Content-Type", "application/json")
                .header("Cookie", postAuthRequest.getToken())
                .when()
                .body(payload.toString())
                .delete("booking/" + id)
                ;
    }

    @Step("Deletar reserva sem autorizacao")
    public Response deletarReservaSemAutorizacao(){
        return given()
                .when()
                .get("booking/1")
                .prettyPeek();
    }

    @Step("Deletar uma reserva que não existe")
    public Response deletarUmaReservaQueNaoExiste(int id, JSONObject payload){

        return given()
                .header("Content-Type", "application/json")
                .header("Cookie", postAuthRequest.getToken())
                .when()
                .body(payload.toString())
                .delete("booking/" + id)
                ;
    }


}
