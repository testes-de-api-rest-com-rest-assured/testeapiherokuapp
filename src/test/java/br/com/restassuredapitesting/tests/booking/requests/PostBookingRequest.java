package br.com.restassuredapitesting.tests.booking.requests;

import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.post;

public class PostBookingRequest {

    @Step("Incluir uma nova reserva")
    public Response incluirNovaReserva(){
        return given()
                .header("Content-Type", "application/json")
                .body("{\n" +
                "       \"firstname\" : \"Janaina\",\n" +
                "       \"lastname\" : \"Lima\",\n" +
                "       \"totalprice\" : 200,\n" +
                "       \"depositpaid\" : true,\n" +
                "       \"bookingdates\" : {\n" +
                "       \"checkin\" : \"2019-01-01\",\n" +
                "       \"checkout\" : \"2020-01-01\"},\n" +
                "       \"additionalneeds\" : \"Breakfast\"}")
                .log().all()
                .post("booking/")
                .prettyPeek();
    }

    @Step("Reserva invalida")
    public Response reservaInvalida(){
        return given()
                 .body("{\n" +
                        "       \"firstname\" : \"Janaina\",\n" +
                        "       \"lastname\" : \"Lima\",\n" +
                        "       \"totalprice\" : 200,\n" +
                        "       \"depositpaid\" : true,\n" +
                        "       \"bookingdates\" : {\n" +
                        "       \"checkin\" : \"2019-01-01\",\n" +
                        "       \"checkout\" : \"2020-01-01\"},\n" +
                        "       \"additionalneeds\" : \"Breakfast\"}")
                .post("booking/")
                .prettyPeek();
    }

    @Step("Enviar Accept Invalido")
    public Response enviarAcceptInvalido(){
        return given()
                .header("Content-Type", "application/json")
                .header("Accept", "teste")
                .body("{\n" +
                        "       \"firstname\" : \"Janaina\",\n" +
                        "       \"lastname\" : \"Lima\",\n" +
                        "       \"totalprice\" : 200,\n" +
                        "       \"depositpaid\" : true,\n" +
                        "       \"bookingdates\" : {\n" +
                        "       \"checkin\" : \"2019-01-01\",\n" +
                        "       \"checkout\" : \"2020-01-01\"},\n" +
                        "       \"additionalneeds\" : \"Breakfast\"}")
                .post("booking/")
                .prettyPeek();
    }

    @Step("Incluir uma nova reserva com parametros a mais no payload")
    public Response incluirNovaReservaMaisParametrosPayload(){
        return given()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "       \"firstname\" : \"Janaina\",\n" +
                        "       \"lastname\" : \"Lima\",\n" +
                        "       \"nomedamae\" : \"Ana\",\n" +
                        "       \"totalprice\" : 200,\n" +
                        "       \"depositpaid\" : true,\n" +
                        "       \"bookingdates\" : {\n" +
                        "       \"checkin\" : \"2019-01-01\",\n" +
                        "       \"checkout\" : \"2020-01-01\"},\n" +
                        "       \"additionalneeds\" : \"Breakfast\"}")
                .post("booking/")
                .prettyPeek();
    }

    @Step("Incluir duas reservas")
    public Response incluirDuasReservas(){
        return given()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "       \"firstname\" : \"Janaina\",\n" +
                        "       \"lastname\" : \"Lima\",\n" +
                        "       \"totalprice\" : 200,\n" +
                        "       \"depositpaid\" : true,\n" +
                        "       \"bookingdates\" : {\n" +
                        "       \"checkin\" : \"2019-01-01\",\n" +
                        "       \"checkout\" : \"2020-01-01\"},\n" +
                        "       \"additionalneeds\" : \"Breakfast\"}")
                .log().all()
                .post("booking/")
                .prettyPeek();
    }

}
