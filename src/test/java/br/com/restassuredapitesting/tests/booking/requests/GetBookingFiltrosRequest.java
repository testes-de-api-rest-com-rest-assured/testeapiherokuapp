package br.com.restassuredapitesting.tests.booking.requests;

import io.qameta.allure.Step;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class GetBookingFiltrosRequest {

    @Step("Buscar reserva com filtro firstname")
    public Response reservaComFiltroFirstname(){
        return given()
                .when()
                .queryParam("firstname", "Mark")
                .get("booking")
                .prettyPeek();
    }

    @Step("Buscar reserva com filtro lastname")
    public Response reservaComFiltroLastname(){
        return given()
                .when()
                .queryParam("lastname", "Smith")
                .get("booking")
                .prettyPeek();
    }

    @Step("Buscar reserva com filtro checkin")
    public Response reservaComFiltroCheckin(){
        return given()
                .header("Accept", "application/json")
                .when()
                .queryParam("checkin", "2018-07-08")
                .get("booking")
                .prettyPeek();
    }

    @Step("Buscar reserva com filtro checkout")
    public Response reservaComFiltroCheckout(){
        return given()
                .when()
                .queryParam("checkout", "2019-09-28")
                .get("booking")
                .prettyPeek();
    }

    @Step("Buscar reserva com filtros checkin e checkout")
    public Response reservaComFiltroCheckinCheckout(){
        return given()
                .when()
                .queryParam("checkin", "2017-12-14")
                .queryParam("checkout", "2020-09-13")
                .get("booking")
                .prettyPeek();
    }

    @Step("Buscar reserva com filtros firstname, checkin e checkout")
    public Response reservaComFiltroFirstnameCheckinCheckout(){
        return given()
                .when()
                .queryParam("firstname", "Mary")
                .queryParam("bookingdates.checkin", "2017-12-14")
                .queryParam("bookingdates.checkout", "2020-09-13")
                .get("booking")
                .prettyPeek();
    }

    @Step("Erro 500 por filtro mal formatado")
    public Response Erro500FiltroMalFormatado(){
        return given()
                .when()
                .queryParam("checkout", "20190928")
                .get("booking")
                .prettyPeek();
    }

}
