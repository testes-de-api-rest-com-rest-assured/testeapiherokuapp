package br.com.restassuredapitesting.tests.booking.tests;

import br.com.restassuredapitesting.suites.Acceptance;
import br.com.restassuredapitesting.suites.E2e;
import br.com.restassuredapitesting.tests.base.tests.BaseTest;
import br.com.restassuredapitesting.tests.booking.requests.GetBookingRequest;
import br.com.restassuredapitesting.tests.booking.requests.PostBookingRequest;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.Matchers.lessThan;

public class PostBookingTest extends BaseTest {

    PostBookingRequest postBookingRequest = new PostBookingRequest();

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(Acceptance.class)
    @DisplayName("Validar inclusao de nova reserva")

    public void validarInclusaoNovaReserva() throws Exception{
        postBookingRequest.incluirNovaReserva().then()
                .statusCode(200)
                .time(lessThan(10L), TimeUnit.SECONDS)
        ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar reserva invalida")

    public void validarReservaInvalida() throws Exception{
        postBookingRequest.reservaInvalida().then()
                .statusCode(500)
                .time(lessThan(10L), TimeUnit.SECONDS)
                ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar erro 418 quando enviar Accept invalido")

    public void validarErro418AcceptInvalido() throws Exception{
        postBookingRequest.enviarAcceptInvalido().then()
                .statusCode(418)
                .time(lessThan(10L), TimeUnit.SECONDS)
        ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar incluir uma nova reserva com parametros a mais no payload")

    public void ValidarIncluirNovaReservaMaisParametrosPayload() throws Exception{
        postBookingRequest.incluirNovaReservaMaisParametrosPayload().then()
                .statusCode(200)
                .time(lessThan(10L), TimeUnit.SECONDS)
        ;
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Category(E2e.class)
    @DisplayName("Validar inclusao duas reservas")

    public void validarInclusaoDuasReservas() throws Exception{
        postBookingRequest.incluirNovaReserva().then()
                .statusCode(200)
                .time(lessThan(10L), TimeUnit.SECONDS)
        ;
        postBookingRequest.incluirNovaReserva().then()
                .statusCode(200)
                .time(lessThan(10L), TimeUnit.SECONDS)
        ;
    }
}
